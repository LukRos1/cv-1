# Klaudia Stępniewska
![ ](http://www.3lo.lublin.pl/strona,pl,filesimage,5391,0,large)
e-mail: klauu15@op.pl
tel.: 500 400 600
adres zamieszkania: ul. Świerkowa 6/5, 20-820 Lublin

## Wykształcenie
**10.2018 - obecnie**
Uniwersytet Marii Curie-Skłodowskiej w Lublinie, studentka 2 roku II stopnia geoinformatyki 

**10.2015 - 07.2018**
Uniwersytet Marii Curie-Skłodowskiej w Lublinie, absolwentka geoinformatyki 
poziom wykształcenia: licencjat 

**09/2012 - 04/2015**
III Liceum Ogólnokształcące im. Unii Lubelskiej w Lublinie 
Klasa z rozszerzoną matematyką, geografią i językiem angielskim 
Poziom wykształcenia: średnie 

## Znajomosć języków obcych
* polski -  _ojczysty_ 
* angielski - _średnio-zaawansowany_ 
* rosyjski - _podstawowy_ 

## Umiejętności
Odpowiedzialność, sumienność, punktualność, dokładność, konsekwencja w dążeniu do celu, bardzo dobry poziom obsługi komputera, w tym, wynikająca z programu studiów znajomość programów: GIS/CAD (Platforma ArcGIS, QGIS, DraftSight)), podstaw programowania w językach: C++, Python, R, PHP, HTML, SQL, podstawowa obsługa Bash, obsługa urządzeń GPS. Ponadto umiejętność szybkiego uczenia się, łatwość w nawiązywaniu nowych kontaktów, praca w zespole, prawo jazdy kat. B, brak nałogów 

## Certyfikaty
**2018**
TLS (Terrestrial Laser Scanning) w teorii i praktyce 
Organizator: ProGea 4D, w ramach projektu &#8220;Geo4work - Rozwój kompetencji zawodowych studentów Wydziału Nauk oZiemi i Gospodarki Przestrzennej UMCS i wzmocnienie ich konkurencyjności na nowoczesnym rynku pracy&#8221; 

**2018**
Przetwarzanie danych LiDAR 
Organizator: ProGea 4D, w ramach projektu &#8220;Geo4work - Rozwój kompetencji zawodowych studentów Wydziału Nauk oZiemi i Gospodarki Przestrzennej UMCS i wzmocnienie ich konkurencyjności na nowoczesnym rynku pracy&#8221; 

**2018**
Podejmowanie decyzji 
Organizator: Lubelskie Centrum Consultingu sp. z o.o., w ramach projektu &#8220;Geo4work - Rozwój kompetencji zawodowych studentów Wydziału Nauk oZiemi i Gospodarki Przestrzennej UMCS i wzmocnienie ich konkurencyjności na nowoczesnym rynku pracy&#8221; 

**2018**
Trening kreatywnego myślenia 
Organizator: Lubelskie Centrum Consultingu sp. z o.o., w ramach projektu &#8220;Geo4work - Rozwój kompetencji zawodowych studentów Wydziału Nauk oZiemi i Gospodarki Przestrzennej UMCS i wzmocnienie ich konkurencyjności na nowoczesnym rynku pracy&#8221; 

**2018**
Trening kreatywności w środowisku laboratorium innowacji (i-lab) 
Organizator: Lubelskie Centrum Consultingu sp. z o.o., w ramach projektu &#8220;Geo4work - Rozwój kompetencji zawodowych studentów Wydziału Nauk oZiemi i Gospodarki Przestrzennej UMCS i wzmocnienie ich konkurencyjności na nowoczesnym rynku pracy&#8221; 

**2018**
Rozwiązywanie konfliktów 
Organizator: Lubelskie Centrum Consultingu sp. z o.o., w ramach projektu &#8220;Geo4work - Rozwój kompetencji zawodowych studentów Wydziału Nauk oZiemi i Gospodarki Przestrzennej UMCS i wzmocnienie ich konkurencyjności na nowoczesnym rynku pracy&#8221; 

**2018**
Wystąpienia publiczne 
Organizator: Lubelskie Centrum Consultingu sp. z o.o., w ramach projektu &#8220;Geo4work - Rozwój kompetencji zawodowych studentów Wydziału Nauk oZiemi i Gospodarki Przestrzennej UMCS i wzmocnienie ich konkurencyjności na nowoczesnym rynku pracy&#8221; 

**2015/2016, 2016/2017 oraz 2017/2018**
Stypendium Rektora dla najlepszych studentów 
Organ przyznający: Rektor UMCS w Lublinie 

**2015/2016**
Stypendium w ramach Programu wspierania laureatów i finalistów zawodów stopnia centralnego olimpiad i turniejów, studiujących na terenie miasta Lublin 
Organ przyznający: UM Lublin 

**06/2016**
Przygotowanie i prowadzenie warsztatów terenowych Geocaching dla młodzieży licealnej 
Organizator: III LO im. Unii Lubelskiej w Lublinie 

**2014/2015**
Laureatka VIII edycji Ogólnopolskiej Olimpiady &#8220;O Diamentowy Indeks AGH&#8221; z przedmiotu geografia z elementami geologii 
Organizator: AGH w Krakowie 

**2014**
Certyfikat ECDL potwierdzający znajomość programów MS Office 
Organizator: ECDL Polska, PTI 

## Aktywność dodatkowa
**10.2016 – obecnie**
KU AZS UMCS 
Dodatkowe informacje: Reprezentacja Uczelni na zawodach wojewódzkich i ogólnopolskich w  narciarstwie alpejskim. 

**10.2015 – obecnie**
Studenckie Koło Naukowe Geoinformatyków &#8220;GeoIT&#8221; 
Dodatkowe informacje: Czynny udział w projektach i festiwalach naukowych 

## Zainteresowania
Fotografia, nowinki technologiczne, sport, podróże 